<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>E-konseling</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <header>
    <div class="inner-width">
      <h1>E-KONSELING</h1>
      <div class="menu-icon">
        <i class="fas fa-align-right"></i>
      </div>
    </div>
  </header>
  <div class="navigation-menu">
    <nav>
      <li><a href="index.php">Home</a></li>
      <li><a href="/konseling/artikel/index.html">Artikel</a></li>
      <li><a href="/konseling/konsultasi/konseling.html">Konsultasi</a></li>
      <li><a href="/konseling/kuis/kuis.html">Quiz</a></li>
      <li><a href="/konseling/kontak/kontak.html">Contact</a></li>
      <li><a href="logout.php">Log Out</a></li>
    </nav>
  </div>

  <p>
   Aplikasi ini membantu orang berkonsultasi kepribadiannya atau kecemasan terhadap dirinya dengan dokter atau psikiater
  </p>

  <script>
    $(".menu-icon").click(function(){
      $(this).toggleClass("active");
      $(".navigation-menu").toggleClass("active");
      $(".menu-icon i").toggleClass("fa-times");
    });
  </script>
</body>
</html>
